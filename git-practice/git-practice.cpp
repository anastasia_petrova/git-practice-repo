// git-practice.cpp: определяет точку входа для консольного приложения.
//

// #include "stdafx.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

static const char *INPUT_FILE = "input.txt";
static const char *OUTPUT_FILE = "output.txt";

template <typename T>
struct functor
{
    T operator()(T& e)
    {
        return e++;
    }
};

template <typename T>
struct predicate
{
    bool operator()(T e)
    {
        return e<0;
    }
};

template <typename T>
struct printer
{
    void operator()(T& e)
    {
        std::cout<<e<<std::endl;
    }
};

template <typename T>
class input_reader 
{
public:
    std::vector<T> operator()(const char* filename)
    {
        std::vector<T> out;

        std::fstream F;
		F.open(filename);
		int k;
		while (!F.eof())
		{
			F>>k;
			out.push_back(k);
		}
		F.close();
		
        return out;
    }
};

template <typename T>
class output_writer
{
public:
    void operator()(const char* filename, std::vector<T>& vec)
    {
        std::ofstream output_file(filename, std::ofstream::trunc);
		for (unsigned int i=0; i<vec.size(); i++)
		{
			output_file<<vec[i]<<std::endl;
		}
    }
};

int main(int argc, char* argv[])
{
    std::vector<int> source_v;

    using curr_type = std::remove_reference<decltype(source_v[0])>::type;

    std::vector<curr_type> target_v;

    input_reader<curr_type> read;
    source_v = read(INPUT_FILE);

    predicate<curr_type> pred;
    std::copy_if(source_v.begin(), source_v.end(), std::back_inserter(target_v), pred);

    functor<curr_type> func;
    std::for_each(target_v.begin(), target_v.end(), func);

    printer<curr_type> printr;
    std::for_each(target_v.begin(), target_v.end(), printr);

    output_writer<curr_type> write;
    write(OUTPUT_FILE, target_v);

    return 0;
}
